import pytest


@pytest.mark.asyncio
async def test_generate_random_string(mock_hub, hub):

    mock_hub.tool.random.id.generate_random_string = (
        hub.tool.random.id.generate_random_string
    )

    ret = mock_hub.tool.random.id.generate_random_string(1)

    assert ret

    # Each character is used to represent 6 bits (log2(64) = 6).
    # Therefore 4 chars are used to represent 4 * 6 = 24 bits = 3 bytes.
    # 4*(n/3) chars are used to represent n bytes, and this needs to be rounded up to a multiple of 4.
    # So for 1 byte, length of string in base64 format will be 4.
    assert len(ret) == 4
