import pytest

from tests.integration.states import run_sls

esm_cache = {}

STATE_ID_PRESENT = """
random_id:
  random.id.present:
    - name: rp
    - length: 1
"""

STATE_ID_ABSENT = """
random_id:
  random.id.absent:
    - name: rp
"""


def test_id_present(hub):
    running = run_sls(STATE_ID_PRESENT, managed_state=esm_cache)
    ret = running["random.id_|-random_id_|-rp_|-present"]
    print(ret["new_state"])
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert len(new_state["output"]) == 4


@pytest.mark.depends(on=["test_id_present"])
def test_id_absent(hub):
    running = run_sls(STATE_ID_ABSENT, managed_state=esm_cache)
    ret = running["random.id_|-random_id_|-rp_|-absent"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"] is None
    assert len(ret["old_state"]["output"]) == 4
